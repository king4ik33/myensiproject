<?php

namespace App\Observers;

use App\Domain\Contents\Actions\ChangeRatingOnVoteCreateAction;
use App\Domain\Contents\Actions\ChangeRatingOnVoteDeleteAction;
use App\Domain\Contents\Actions\ChangeRatingOnVoteUpdateAction;
use App\Domain\Contents\Models\Vote;

class VoteObserver
{
    /**
     * Handle the Vote "created" event.
     *
     * @param Vote $vote
     * @return void
     */
    public function created(Vote $vote): void
    {
        $action = new ChangeRatingOnVoteCreateAction();
        $action->execute($vote);
    }

    /**
     * Handle the Vote "updated" event.
     *
     * @param Vote $vote
     * @return void
     */
    public function updated(Vote $vote): void
    {
        if ($vote->isDirty('vote')) {
            $action = new ChangeRatingOnVoteUpdateAction();
            $action->execute($vote);
        }
    }

    /**
     * Handle the Vote "deleted" event.
     *
     * @param Vote $vote
     * @return void
     */
    public function deleted(Vote $vote): void
    {
        $action = new ChangeRatingOnVoteDeleteAction();
        $action->execute($vote);
    }

    /**
     * Handle the Vote "restored" event.
     *
     * @param Vote $vote
     * @return void
     */
    public function restored(Vote $vote): void
    {
        $action = new ChangeRatingOnVoteUpdateAction();
        $action->execute($vote);
    }

    /**
     * Handle the Vote "force deleted" event.
     *
     * @param Vote $vote
     * @return void
     */
    public function forceDeleted(Vote $vote): void
    {
        $action = new ChangeRatingOnVoteDeleteAction();
        $action->execute($vote);
    }
}
