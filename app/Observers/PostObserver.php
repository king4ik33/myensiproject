<?php

namespace App\Observers;

use App\Domain\Contents\Actions\CreateOrUpdateIndexElasticAction;
use App\Domain\Contents\Actions\DeleteIndexElasticAction;
use Elasticsearch\Client;

class PostObserver
{
    private Client $elasticsearch;
    public function __construct(Client $elasticsearch)
    {
        $this->elasticsearch = $elasticsearch;
    }
    public function created($model): void
    {
        $action = new CreateOrUpdateIndexElasticAction($this->elasticsearch);
        $action->execute($model);
    }

    public function updated($model): void
    {
        $action = new CreateOrUpdateIndexElasticAction($this->elasticsearch);
        $action->execute($model);
    }

    public function restored($model): void
    {
        $action = new CreateOrUpdateIndexElasticAction($this->elasticsearch);
        $action->execute($model);
    }

    public function deleted($model): void
    {
        $action = new DeleteIndexElasticAction($this->elasticsearch);
        $action->execute($model);
    }
}
