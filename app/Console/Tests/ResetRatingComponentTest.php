<?php

use App\Domain\Contents\Models\Post;
use App\Domain\Contents\Models\Vote;
use App\Http\ApiV1\OpenApiGenerated\Enums\VotesVoteEnum;
use Illuminate\Database\Eloquent\Collection;
use function Pest\Laravel\artisan;

use Tests\ComponentTestCase;
use function Pest\Laravel\assertDatabaseHas;
use function Pest\Laravel\assertDatabaseMissing;
use function PHPUnit\Framework\assertNotEquals;

uses(ComponentTestCase::class);
uses()->group('component');

test("Command posts:reset-rating {post_id} success", function () {
    /** @var Post $post */
    $post = Post::factory()->createOne();

    /** @var Collection $votes */
    $votes = Vote::factory()->count(10)->create([
        'post_id' => $post->id,
        'vote' => VotesVoteEnum::AGAINST->value,
    ]);

    /** @var Post $postElement */
    $postElement = Post::query()->where('id', $post->id)->first();

    assertNotEquals(0, $postElement->rating);

    artisan("posts:reset-rating {$post->id}")->assertSuccessful();

    assertDatabaseMissing((new Vote())->getTable(), [
        'id' => [
            $votes->first()->id,
            $votes->last()->id,
        ]
    ]);
    assertDatabaseHas((new Post())->getTable(), [
        'id' => $post->id,
        'rating' => 0,
    ]);
});
