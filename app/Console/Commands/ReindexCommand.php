<?php

namespace App\Console\Commands;

use App\Domain\Contents\Models\Post;
use Elasticsearch\Client;
use Illuminate\Console\Command;

class ReindexCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'posts:reindex';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Indexes all posts to Elasticsearch';

    private Client $elasticsearch;

    public function __construct(Client $elasticsearch)
    {
        parent::__construct();

        $this->elasticsearch = $elasticsearch;
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->info('Indexing all posts. This might take a while...');
        $chunkSize = 100;

        try {
            Post::query()->with(['tags', 'hubs'])->chunk($chunkSize, function ($posts) {
                $params = [];
                /** @var Post $post */
                foreach ($posts as $post) {
                    $params['body'][] = ['index' => [
                        '_index' => $post->getSearchIndex()
                    ]];
                    $params['body'][] = $post->toSearchArray();
                }
                $this->elasticsearch->bulk($params);

                $this->output->write('.');
            });
        } catch (\Throwable $e) {
            $this->info('\nError!');
            $this->info("\n{$e->getMessage()}");
        }

        $this->info('\nDone!');

        return Command::SUCCESS;
    }
}
