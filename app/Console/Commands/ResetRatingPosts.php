<?php

namespace App\Console\Commands;

use App\Domain\Contents\Actions\DeleteVoteByPostIdAction;
use Illuminate\Console\Command;

class ResetRatingPosts extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'posts:reset-rating {post_id}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Reset posts rating';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(DeleteVoteByPostIdAction $action)
    {
        $postId = $this->argument('post_id');

        if (empty($postId)) {
            $this->error('Post id is empty');
            return Command::INVALID;
        }

        $action->execute($postId);

        $this->info('Successful!');
        return Command::SUCCESS;
    }
}
