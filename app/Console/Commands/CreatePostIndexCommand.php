<?php

namespace App\Console\Commands;

use App\Domain\Contents\Models\Post;
use App\Http\ApiV1\Resources\HubsResource;
use App\Http\ApiV1\Resources\TagsResource;
use Elasticsearch\Client;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class CreatePostIndexCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'posts:create-index {reset}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create index for posts to Elasticsearch';

    private Client $elasticsearch;

    public function __construct(Client $elasticsearch)
    {
        parent::__construct();

        $this->elasticsearch = $elasticsearch;
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $params = $this->getParams();

        $reset = $this->argument('reset');

        if (!empty($reset)) {
            try {
                $this->elasticsearch->indices()->delete(['index' => $params['index']]);
            } catch (\Exception) {

            }
        }

        $this->elasticsearch->indices()->create($params);

        return Command::SUCCESS;
    }

    private function getParams(): array
    {
        return [
            'index' => 'posts',
            'body' => [
                'settings' => [
                    'number_of_shards' => 3,
                    'number_of_replicas' => 1
                ],
                'mappings' => [
                    'properties' => [
                        'id' => [
                            'type' => 'keyword'
                        ],
                        'title_text' => [
                            'type' => 'text'
                        ],
                        'body_text' => [
                            'type' => 'text'
                        ],
                        'user_id' => [
                            'type' => 'keyword'
                        ],
                        'rating' => [
                            'type' => 'integer',
                        ],
                        'created_at' => [
                            'type' => 'date',
                        ],
                        'updated_at' => [
                            'type' => 'date',
                        ],
                        'tags' => [
                            'properties' => [
                                'id' => [
                                    'type' => 'keyword'
                                ],
                                'title' => [
                                    'type' => 'keyword'
                                ],
                            ]
                        ],
                        'hubs' => [
                            'properties' => [
                                'id' => [
                                    'type' => 'keyword'
                                ],
                                'title' => [
                                    'type' => 'keyword'
                                ],
                            ]
                        ]
                    ]
                ]
            ]
        ];
    }
}
