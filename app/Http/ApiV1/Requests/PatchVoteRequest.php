<?php

namespace App\Http\ApiV1\Requests;

use App\Http\ApiV1\OpenApiGenerated\Enums\VotesVoteEnum;
use App\Http\ApiV1\Support\Requests\BaseFormRequest;
use Illuminate\Validation\Rule;
use Illuminate\Validation\Rules\Enum;

class PatchVoteRequest extends BaseFormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'vote' => ['required', 'string', new Enum(VotesVoteEnum::class)],
        ];
    }
}
