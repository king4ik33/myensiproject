<?php

namespace App\Http\ApiV1\Requests;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;

class CreatePostRequest extends BaseFormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'title' => ['string', 'required'],
            'body' => ['string', 'required'],
            'user_id' => ['integer', 'required'],
            'tags' => ['nullable', 'array', 'min:1'],
            'tags.*' => ['integer', 'required_with:tags'],
            'hubs' => ['nullable', 'array', 'min:1'],
            'hubs.*' => ['integer', 'required_with:hubs'],
        ];
    }
}
