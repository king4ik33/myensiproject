<?php

namespace App\Http\ApiV1\Requests;

use App\Http\ApiV1\OpenApiGenerated\Enums\VotesVoteEnum;
use App\Http\ApiV1\Support\Requests\BaseFormRequest;
use Illuminate\Validation\Rules\Enum;

class CreateVoteRequest extends BaseFormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'post_id' => ['integer', 'required'],
            'user_id' => ['integer', 'required'],
            'vote' => ['required', 'string', new Enum(VotesVoteEnum::class)],
        ];
    }
}
