<?php

namespace App\Http\ApiV1\Resources;

use App\Domain\Contents\Models\Tag;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/** @mixin Tag */
class TagsResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'title' => $this->title,

            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];

    }
}
