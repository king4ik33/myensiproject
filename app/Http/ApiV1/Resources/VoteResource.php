<?php

namespace App\Http\ApiV1\Resources;

use App\Domain\Contents\Models\Vote;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/**
 * @mixin Vote
 */
class VoteResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'post_id' => $this->post_id,
            'user_id' => $this->user_id,
            'vote' => $this->vote,

            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];

    }
}
