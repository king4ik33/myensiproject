<?php

namespace App\Http\ApiV1\Resources;

use App\Domain\Contents\Models\Post;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/** @mixin Post */
class PostResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'body' => $this->body,
            'user_id' => $this->user_id,
            'rating' => $this->rating,

            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,

            'tags' => TagsResource::collection($this->whenLoaded('tags')),
            'hubs' => HubsResource::collection($this->whenLoaded('hubs'))
        ];

    }
}
