<?php

namespace App\Http\ApiV1\Resources;

use App\Domain\Contents\Models\Hub;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/** @mixin Hub */
class HubsResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'title' => $this->title,

            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];

    }
}
