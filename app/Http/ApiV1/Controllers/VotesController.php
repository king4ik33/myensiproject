<?php

namespace App\Http\ApiV1\Controllers;

use App\Domain\Contents\Actions\CreateVoteAction;
use App\Domain\Contents\Actions\DeleteVoteAction;
use App\Domain\Contents\Actions\PatchVoteAction;
use App\Domain\Contents\Actions\ReplaceVoteAction;
use App\Http\ApiV1\Queries\VotesQuery;
use App\Http\ApiV1\Requests\CreateVoteRequest;
use App\Http\ApiV1\Requests\PatchVoteRequest;
use App\Http\ApiV1\Requests\ReplaceVoteRequest;
use App\Http\ApiV1\Resources\VoteResource;
use App\Http\ApiV1\Support\Pagination\PageBuilderFactory;
use App\Http\ApiV1\Support\Resources\EmptyResource;

class VotesController
{
    public function create(CreateVoteRequest $request, CreateVoteAction $action): VoteResource
    {
        return new VoteResource($action->execute($request->validated()));
    }

    public function get(int $id, VotesQuery $query): VoteResource
    {
        return new VoteResource($query->findOrFail($id));
    }

    public function replace(int $id, ReplaceVoteRequest $request, ReplaceVoteAction $action): VoteResource
    {
        return new VoteResource($action->execute($id, $request->validated()));
    }

    public function delete(int $id, DeleteVoteAction $action): EmptyResource
    {
        $action->execute($id);

        return new EmptyResource();
    }

    public function patch(int $id, PatchVoteRequest $request, PatchVoteAction $action): VoteResource
    {
        return new VoteResource($action->execute($id, $request->validated()));
    }

    public function search(PageBuilderFactory $pageBuilderFactory, VotesQuery $query)
    {
        return VoteResource::collectPage(
            $pageBuilderFactory->fromQuery($query)->build()
        );
    }
}
