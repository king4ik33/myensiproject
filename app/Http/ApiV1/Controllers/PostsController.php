<?php

namespace App\Http\ApiV1\Controllers;

use App\Domain\Contents\Actions\CreatePostAction;
use App\Domain\Contents\Actions\DeletePostAction;
use App\Domain\Contents\Actions\PatchPostAction;
use App\Domain\Contents\Actions\ReplacePostAction;
use App\Http\ApiV1\Queries\PostsQuery;
use App\Http\ApiV1\Requests\CreatePostRequest;
use App\Http\ApiV1\Requests\PatchPostRequest;
use App\Http\ApiV1\Requests\ReplacePostRequest;
use App\Http\ApiV1\Resources\PostResource;
use App\Http\ApiV1\Support\Pagination\PageBuilderFactory;
use App\Http\ApiV1\Support\Resources\EmptyResource;

class PostsController
{
    public function create(CreatePostRequest $request, CreatePostAction $action): PostResource
    {
        return new PostResource($action->execute($request->validated()));
    }

    public function get(int $id, PostsQuery $query): PostResource
    {
        return new PostResource($query->findOrFail($id));
    }

    public function replace(int $id, ReplacePostRequest $request, ReplacePostAction $action): PostResource
    {
        return new PostResource($action->execute($id, $request->validated()));
    }

    public function delete(int $id, DeletePostAction $action): EmptyResource
    {
        $action->execute($id);

        return new EmptyResource();
    }

    public function patch(int $id, PatchPostRequest $request, PatchPostAction $action): PostResource
    {
        return new PostResource($action->execute($id, $request->validated()));
    }

    public function search(PageBuilderFactory $pageBuilderFactory, PostsQuery $query)
    {
        return PostResource::collectPage(
            $pageBuilderFactory->fromQuery($query)->build()
        );
    }
}
