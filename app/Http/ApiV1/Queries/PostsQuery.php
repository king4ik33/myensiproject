<?php

namespace App\Http\ApiV1\Queries;

use App\Domain\Contents\Models\Post;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

class PostsQuery extends QueryBuilder
{
    public function __construct()
    {
        $query = Post::query();

        parent::__construct($query);

        $this->allowedIncludes([
            'hubs',
            'tags',
        ]);

        $this->allowedSorts([
            'id',
            'title',
            'user_id',
            'created_at',
            'updated_at',
        ]);

        $this->defaultSort('id');

        $this->allowedFilters([
            AllowedFilter::exact('id'),
            AllowedFilter::exact('title'),
            AllowedFilter::exact('user_id'),

            AllowedFilter::partial('title_like', 'title'),
        ]);

        $this->defaultSort('id');
    }
}
