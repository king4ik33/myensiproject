<?php

namespace App\Http\ApiV1\Queries;

use App\Domain\Contents\Models\Vote;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

class VotesQuery extends QueryBuilder
{
    public function __construct()
    {
        parent::__construct(Vote::query());

        $this->allowedSorts([
            'id',
            'post_id',
            'user_id',
            'created_at',
            'updated_at',
        ]);

        $this->defaultSort('id');

        $this->allowedFilters([
            AllowedFilter::exact('id'),
            AllowedFilter::exact('post_id'),
            AllowedFilter::exact('user_id'),
        ]);

    }
}
