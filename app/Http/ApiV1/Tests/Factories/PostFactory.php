<?php

namespace App\Http\ApiV1\Tests\Factories;

use App\Http\ApiV1\Support\Tests\Factories\BaseApiFactory;

class PostFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'title' => $this->faker->text(20),
            'body' => $this->faker->text(200),
            'user_id' => $this->foreignId(),
        ];
    }

    public function make(array $extra = []): array
    {
        return $this->makeArray($extra);
    }
}
