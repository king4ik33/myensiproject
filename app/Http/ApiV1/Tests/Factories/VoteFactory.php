<?php

namespace App\Http\ApiV1\Tests\Factories;

use App\Http\ApiV1\OpenApiGenerated\Enums\VotesVoteEnum;
use App\Http\ApiV1\Support\Tests\Factories\BaseApiFactory;

class VoteFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'post_id' => $this->foreignId(),
            'user_id' => $this->foreignId(),
            'vote' => $this->faker->randomElement(VotesVoteEnum::cases())->value,
        ];
    }

    public function make(array $extra = []): array
    {
        return $this->makeArray($extra);
    }
}
