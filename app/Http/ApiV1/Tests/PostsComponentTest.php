<?php

use App\Domain\Contents\Models\Post;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;

use App\Http\ApiV1\Tests\Factories\PostFactory;
use function Pest\Laravel\assertDatabaseHas;
use function Pest\Laravel\assertDatabaseMissing;
use function Pest\Laravel\deleteJson;
use function Pest\Laravel\getJson;
use function Pest\Laravel\patchJson;
use function Pest\Laravel\postJson;
use function Pest\Laravel\putJson;

uses(ApiV1ComponentTestCase::class);
uses()->group('posts', 'component');

test('POST /api/v1/posts 201', function () {
    $postRequest = PostFactory::new()->make();

    postJson('/api/v1/posts', $postRequest)
        ->assertStatus(201)
        ->assertJsonPath('data.title', $postRequest['title'])
        ->assertJsonPath('data.body', $postRequest['body']);

    assertDatabaseHas('posts', [
        'title' => $postRequest['title'],
        'body' => $postRequest['body'],
        'user_id' => $postRequest['user_id'],
        'rating' => 0,
    ]);
});

test('POST /api/v1/posts 400', function () {
    $postRequest = PostFactory::new()->make(['title' => 33333]);

    $this->skipNextOpenApiRequestValidation();

    postJson('/api/v1/posts', $postRequest)
        ->assertStatus(400);
});

test('GET /api/v1/posts/{id} 200', function () {
    /** @var Post $post */
    $post = Post::factory()->create();

    getJson("/api/v1/posts/{$post->id}")
        ->assertStatus(200);
});

test('GET /api/v1/posts/{id} 404', function () {
    $postId = 404;

    getJson("/api/v1/posts/$postId")
        ->assertStatus(404);
});

test('PUT /api/v1/posts/{id} 200', function () {
    /** @var Post $post */
    $post = Post::factory()->create();

    $postRequest = PostFactory::new()->make();

    putJson("/api/v1/posts/{$post->id}", $postRequest)
        ->assertStatus(200)
        ->assertJsonPath('data.title', $postRequest['title'])
        ->assertJsonPath('data.body', $postRequest['body']);

    assertDatabaseHas('posts', [
        'id' => $post->id,
        'title' => $postRequest['title'],
        'body' => $postRequest['body'],
        'user_id' => $post->user_id,
    ]);
});

test('PUT /api/v1/posts/{id} 400', function () {
    /** @var Post $post */
    $post = Post::factory()->create();

    $postRequest = PostFactory::new()->make(['title' => 33333]);

    $this->skipNextOpenApiRequestValidation();

    putJson("/api/v1/posts/{$post->id}", $postRequest)
        ->assertStatus(400);
});

test('PUT /api/v1/posts/{id} 404', function () {
    $postId = 404;
    $postRequest = PostFactory::new()->make();

    putJson("/api/v1/posts/$postId", $postRequest)
        ->assertStatus(404);
});

test('DELETE /api/v1/posts/{id} 200', function () {
    /** @var Post $post */
    $post = Post::factory()->create();

    deleteJson("/api/v1/posts/{$post->id}")
        ->assertStatus(200);

    assertDatabaseMissing((new Post())->getTable(), [
        'id' => $post->id,
    ]);
});

test('PATCH /api/v1/posts/{id} 200', function () {
    $newTitle = 'New title';
    /** @var Post $post */
    $post = Post::factory()->create();

    patchJson("/api/v1/posts/{$post->id}", ['title' => $newTitle])
        ->assertStatus(200)
        ->assertJsonPath('data.title', $newTitle);

    assertDatabaseHas('posts', [
        'id' => $post->id,
        'title' => $newTitle,
        'body' => $post->body,
        'user_id' => $post->user_id,
    ]);
});

test('PATCH /api/v1/posts/{id} 400', function () {
    /** @var Post $post */
    $post = Post::factory()->create();

    $postRequest = PostFactory::new()->make(['title' => 33333]);

    $this->skipNextOpenApiRequestValidation();

    patchJson("/api/v1/posts/{$post->id}", $postRequest)
        ->assertStatus(400);
});

test('PATCH /api/v1/posts/{id} 404', function () {
    $postId = 404;

    $postRequest = PostFactory::new()->make();

    patchJson("/api/v1/posts/$postId", $postRequest)
        ->assertStatus(404);
});

test('POST /api/v1/posts:search 200', function () {
    $posts = Post::factory()
        ->count(10)
        ->sequence(
            ['user_id' => 100],
            ['user_id' => 300],
        )
        ->create();

    postJson('/api/v1/posts:search', [
        'filter' => ['user_id' => 300],
        'sort'   => ['-id']
    ])
        ->assertStatus(200)
        ->assertJsonCount(5, 'data')
        ->assertJsonPath('data.0.id', $posts->last()->id)
        ->assertJsonPath('data.0.user_id', 300);
});
