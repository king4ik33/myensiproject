<?php

use App\Domain\Contents\Models\Post;
use App\Domain\Contents\Models\Vote;
use App\Http\ApiV1\OpenApiGenerated\Enums\VotesVoteEnum;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;

use App\Http\ApiV1\Tests\Factories\VoteFactory;
use function Pest\Laravel\assertDatabaseHas;
use function Pest\Laravel\assertDatabaseMissing;
use function Pest\Laravel\deleteJson;
use function Pest\Laravel\getJson;
use function Pest\Laravel\patchJson;
use function Pest\Laravel\postJson;
use function Pest\Laravel\putJson;

uses(ApiV1ComponentTestCase::class);
uses()->group('component');

test('POST /api/v1/votes 201', function () {
    /** @var Post $post */
    $post = Post::factory()->createOne();

    $voteRequest = VoteFactory::new()->make(['post_id' => $post->id]);

    postJson('/api/v1/votes', $voteRequest)
        ->assertStatus(201)
        ->assertJsonPath('data.post_id', $voteRequest['post_id'])
        ->assertJsonPath('data.user_id', $voteRequest['user_id'])
        ->assertJsonPath('data.vote', $voteRequest['vote']);

    assertDatabaseHas((new Vote())->getTable(), [
        'post_id' => $voteRequest['post_id'],
        'user_id' => $voteRequest['user_id'],
        'vote' => $voteRequest['vote'],
    ]);
});

test('POST /api/v1/votes 400', function () {
    $voteRequest = VoteFactory::new()->make(['vote' => 1]);

    $this->skipNextOpenApiRequestValidation();

    postJson('/api/v1/votes', $voteRequest)
        ->assertStatus(400);
});

test('GET /api/v1/votes/{id} 200', function () {
    /** @var Post $post */
    $post = Post::factory()->createOne();

    /** @var Vote $vote */
    $vote = Vote::factory()->create(['post_id' => $post->id]);

    getJson("/api/v1/votes/{$vote->id}")
        ->assertStatus(200);
});

test('GET /api/v1/votes/{id} 404', function () {
    $voteId = 404;

    getJson("/api/v1/votes/{$voteId}")
        ->assertStatus(404);
});

test('PUT /api/v1/votes/{id} 200', function () {
    $newVoteValue = VotesVoteEnum::AGAINST->value;
    /** @var Post $post */
    $post = Post::factory()->createOne();

    /** @var Vote $vote */
    $vote = Vote::factory()->create([
        'post_id' => $post->id,
        'vote' => VotesVoteEnum::FOR->value,
    ]);

    putJson("/api/v1/votes/{$vote->id}", ['vote' => $newVoteValue])
        ->assertStatus(200)
        ->assertJsonPath('data.post_id', $vote->post_id)
        ->assertJsonPath('data.user_id', $vote->user_id)
        ->assertJsonPath('data.vote', $newVoteValue);

    assertDatabaseHas((new Vote())->getTable(), [
        'id' => $vote->id,
        'user_id' => $vote->user_id,
        'post_id' => $vote->post_id,
        'vote' => $newVoteValue,
    ]);
});

test('PUT /api/v1/votes/{id} 400', function () {
    $newVoteValue = 'super dislike';
    /** @var Post $post */
    $post = Post::factory()->createOne();

    /** @var Vote $vote */
    $vote = Vote::factory()->create([
        'post_id' => $post->id,
        'vote' => VotesVoteEnum::FOR->value,
    ]);

    $this->skipNextOpenApiRequestValidation();

    putJson("/api/v1/votes/{$vote->id}", ['vote' => $newVoteValue])
        ->assertStatus(400);
});

test('PUT /api/v1/votes/{id} 404', function () {
    $voteId = 404;

    $request = VoteFactory::new()->make();

    putJson("/api/v1/votes/{$voteId}", $request)
        ->assertStatus(404);
});

test('DELETE /api/v1/votes/{id} 200', function () {
    /** @var Post $post */
    $post = Post::factory()->createOne();

    /** @var Vote $vote */
    $vote = Vote::factory()->create([
        'post_id' => $post->id,
        'vote' => VotesVoteEnum::FOR->value,
    ]);

    deleteJson("/api/v1/votes/{$vote->id}")
        ->assertStatus(200);

    assertDatabaseMissing((new Vote())->getTable(), [
        'id' => $vote->id,
    ]);
});

test('PATCH /api/v1/votes/{id} 200', function () {
    $newVoteValue = 'dislike';
    /** @var Post $post */
    $post = Post::factory()->createOne();

    /** @var Vote $vote */
    $vote = Vote::factory()->create([
        'post_id' => $post->id,
        'vote' => VotesVoteEnum::FOR->value,
    ]);

    patchJson("/api/v1/votes/{$vote->id}", ['vote' => $newVoteValue])
        ->assertStatus(200)
        ->assertJsonPath('data.post_id', $vote->post_id)
        ->assertJsonPath('data.user_id', $vote->user_id)
        ->assertJsonPath('data.vote', $newVoteValue);

    assertDatabaseHas((new Vote())->getTable(), [
        'id' => $vote->id,
        'user_id' => $vote->user_id,
        'post_id' => $vote->post_id,
        'vote' => $newVoteValue,
    ]);
});

test('PATCH /api/v1/votes/{id} 400', function () {
    $newVoteValue = 'super dislike';
    /** @var Post $post */
    $post = Post::factory()->createOne();

    /** @var Vote $vote */
    $vote = Vote::factory()->create([
        'post_id' => $post->id,
        'vote' => VotesVoteEnum::FOR->value,
    ]);

    $this->skipNextOpenApiRequestValidation();

    patchJson("/api/v1/votes/{$vote->id}", ['vote' => $newVoteValue])
        ->assertStatus(400);
});

test('PATCH /api/v1/votes/{id} 404', function () {
    $voteId = 404;

    $request = VoteFactory::new()->make();

    patchJson("/api/v1/votes/{$voteId}", $request)
        ->assertStatus(404);
});

test('POST /api/v1/votes:search 200', function () {
    /** @var Post $post */
    $post = Post::factory()->createOne();
    /** @var Vote $vote */
    $votes = Vote::factory()
        ->count(10)
        ->sequence(
            ['user_id' => 100],
            ['user_id' => 300],
        )
        ->create(['post_id' => $post->id]);

    postJson('/api/v1/votes:search', [
        'filter' => ['user_id' => 300],
        'sort'   => ['-id']
    ])
        ->assertStatus(200)
        ->assertJsonCount(5, 'data')
        ->assertJsonPath('data.0.id', $votes->last()->id)
        ->assertJsonPath('data.0.user_id', 300);
});
