<?php

namespace App\Domain\Contents\Actions;

use Elasticsearch\Client;
use Illuminate\Database\Eloquent\Model;

class CreateOrUpdateIndexElasticAction
{
    public function __construct(private readonly Client $elasticsearch)
    {
    }

    public function execute(Model $model): void
    {
        $this->elasticsearch->index([
            'index' => $model->getSearchIndex(),
            'type' => $model->getSearchType(),
            'id' => $model->getKey(),
            'body' => $model->toSearchArray(),
        ]);
    }
}
