<?php

namespace App\Domain\Contents\Actions;

use Elasticsearch\Client;
use Illuminate\Database\Eloquent\Model;

class DeleteIndexElasticAction
{
    public function __construct(private readonly Client $elasticsearch)
    {
    }

    public function execute(Model $model): void
    {
        $this->elasticsearch->delete([
            'index' => $model->getSearchIndex(),
            'type' => $model->getSearchType(),
            'id' => $model->getKey(),
        ]);
    }
}
