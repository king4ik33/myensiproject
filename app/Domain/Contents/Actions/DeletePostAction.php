<?php

namespace App\Domain\Contents\Actions;

use App\Domain\Contents\Models\Post;

class DeletePostAction
{
    public function execute(int $id): void
    {
        Post::destroy($id);
    }
}
