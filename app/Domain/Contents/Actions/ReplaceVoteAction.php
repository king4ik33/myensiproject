<?php

namespace App\Domain\Contents\Actions;

use App\Domain\Contents\Models\Vote;

class ReplaceVoteAction
{
    public function execute(int $id, array $fields): Vote
    {
        $model = Vote::findOrFail($id);
        $model->fill($fields);
        $model->save();

        return $model;
    }
}
