<?php

namespace App\Domain\Contents\Actions;

use App\Domain\Contents\Models\Post;

class ReplacePostAction
{
    public function execute(int $id, array $fields): Post
    {
        $post = Post::findOrFail($id);
        $post->fill($fields);
        $post->save();

        if (!empty($fields['tags'])) {
            $post->tags()->sync($fields['tags']);
        }

        if (!empty($fields['hubs'])) {
            $post->hubs()->sync($fields['hubs']);
        }

        return $post;
    }
}
