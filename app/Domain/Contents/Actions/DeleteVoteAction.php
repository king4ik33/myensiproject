<?php

namespace App\Domain\Contents\Actions;

use App\Domain\Contents\Models\Vote;

class DeleteVoteAction
{
    public function execute(int $id): void
    {
        Vote::destroy($id);
    }
}
