<?php

namespace App\Domain\Contents\Actions;

use App\Domain\Contents\Models\Post;
use App\Domain\Contents\Models\Vote;
use App\Http\ApiV1\OpenApiGenerated\Enums\VotesVoteEnum;

class ChangeRatingOnVoteCreateAction
{
    public function execute(Vote $vote): void
    {
        $voteValue = $vote->vote;
        $postId = $vote->post_id;

        $post = Post::findOrFail($postId);
        $post->rating = ($voteValue == VotesVoteEnum::FOR) ? $post->rating + 1 : $post->rating - 1;
        $post->save();
    }
}
