<?php

namespace App\Domain\Contents\Actions;

use App\Domain\Contents\Models\Post;
use Illuminate\Support\Arr;

class CreatePostAction
{
    public function execute(array $fields): Post
    {
        /** @var Post $post */
        $post = Post::create(Arr::only($fields, Post::FILLABLE));

        if (!empty($fields['tags'])) {
            $post->tags()->attach($fields['tags']);
        }

        if (!empty($fields['hubs'])) {
            $post->hubs()->attach($fields['hubs']);
        }

        return $post;
    }
}
