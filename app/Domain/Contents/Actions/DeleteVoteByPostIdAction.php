<?php

namespace App\Domain\Contents\Actions;

use App\Domain\Contents\Models\Post;
use App\Domain\Contents\Models\Vote;
use Illuminate\Support\Facades\DB;

class DeleteVoteByPostIdAction
{
    public function execute(int $postId): void
    {
        DB::transaction(function () use ($postId) {
            // Удаляем голоса пользователей
            Vote::query()
                ->where('post_id', $postId)
                ->delete();

            // Обнуляем рейтинг поста
            $post = Post::findOrFail($postId);
            $post->rating = 0;
            $post->save();
        });
    }
}
