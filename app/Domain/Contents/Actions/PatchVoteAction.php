<?php

namespace App\Domain\Contents\Actions;

use App\Domain\Contents\Models\Vote;

class PatchVoteAction
{
    public function execute(int $id, array $fields): Vote
    {
        $model = Vote::findOrFail($id);
        $model->update($fields);

        return $model;
    }
}
