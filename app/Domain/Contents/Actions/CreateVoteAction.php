<?php

namespace App\Domain\Contents\Actions;

use App\Domain\Contents\Models\Vote;
use Symfony\Component\Finder\Exception\AccessDeniedException;

class CreateVoteAction
{
    public function execute(array $fields)
    {
        $existVote = Vote::query()
            ->where('post_id', $fields['post_id'])
            ->where('user_id', $fields['user_id'])
            ->first();

        if (!empty($existVote)) {
            return new AccessDeniedException();
        }

        $model = new Vote();
        $model->fill($fields);
        $model->save();

        return $model;
    }
}
