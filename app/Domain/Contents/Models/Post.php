<?php

namespace App\Domain\Contents\Models;

use App\Domain\Contents\Models\Tests\Factories\PostFactory;
use App\Observers\PostObserver;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Collection;
use Illuminate\Support\Carbon;

/**
 * @property int $id
 * @property string $title
 * @property string $body
 * @property int $user_id
 *
 * @property Carbon $created_at
 * @property Carbon $updated_at
 *
 * @property-read int $rating
 * @property Collection|Vote[] $votes
 * @property Collection|Tag[] $tags
 * @property Collection|Hub[] $hubs
 */
class Post extends Model
{
    use HasFactory;

    const FILLABLE = ['title', 'body', 'user_id'];
    protected $fillable = self::FILLABLE;
    protected $hidden = ['pivot'];

    public function votes(): HasMany
    {
        return $this->hasMany(Vote::class, 'post_id');
    }

    public function tags(): BelongsToMany
    {
        return $this->belongsToMany(Tag::class);
    }

    public function hubs(): BelongsToMany
    {
        return $this->belongsToMany(Hub::class);
    }

    public static function factory(): PostFactory
    {
        return PostFactory::new();
    }

    public static function boot(): void
    {
        parent::boot();
        static::observe(PostObserver::class);
    }

    public function getSearchIndex(): string
    {
        return $this->getTable();
    }

    public function getSearchType()
    {
        if (property_exists($this, 'useSearchType')) {
            return $this->useSearchType;
        }
        return $this->getTable();
    }

    public function toSearchArray(): array
    {
        $post = [
            'id' => $this->id,
            'title_text' => $this->title,
            'body_text' => $this->body,
            'user_id' => $this->user_id,
            'rating' => $this->rating,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at
        ];
        foreach ($this->tags as $tag) {
            $arrTag['id'] = $tag['id'];
            $arrTag['title'] = $tag['title'];
            $post['tags'][] = $arrTag;
        }
        foreach ($this->hubs as $hub) {
            $arrHub['id'] = $hub['id'];
            $arrHub['title'] = $hub['title'];
            $post['hubs'][] = $arrHub;
        }
        return $post;
    }
}
