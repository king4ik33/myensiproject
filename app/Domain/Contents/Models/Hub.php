<?php

namespace App\Domain\Contents\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;

/**
 * @property int $id
 * @property string $title
 *
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property Collection|Post[] $posts
 */
class Hub extends Model
{
    use HasFactory;

    protected $fillable = ['title'];
    protected $hidden = ['pivot'];

    public function posts(): BelongsToMany
    {
        return $this->belongsToMany(Post::class);
    }
}
