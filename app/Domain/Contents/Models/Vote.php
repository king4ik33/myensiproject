<?php

namespace App\Domain\Contents\Models;

use App\Domain\Contents\Models\Tests\Factories\VoteFactory;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;

/**
 * @property int $id
 * @property int $post_id
 * @property int $user_id
 * @property string $vote
 *
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property Collection|Post[] $posts
 */
class Vote extends Model
{
    use HasFactory;

    protected $fillable = ['post_id', 'user_id', 'vote'];

    public function posts(): BelongsToMany
    {
        return $this->belongsToMany(Post::class);
    }

    public static function factory(): VoteFactory
    {
        return VoteFactory::new();
    }
}
