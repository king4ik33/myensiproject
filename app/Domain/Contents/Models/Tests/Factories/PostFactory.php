<?php

namespace App\Domain\Contents\Models\Tests\Factories;

use App\Domain\Contents\Models\Post;
use Illuminate\Database\Eloquent\Factories\Factory;

class PostFactory extends Factory
{
    protected $model = Post::class;

    public function definition()
    {
        return [
            'title' => $this->faker->text(20),
            'body' => $this->faker->text(100),
            'user_id' => $this->faker->numberBetween(1),
        ];
    }
}
