<?php

namespace App\Domain\Contents\Models\Tests\Factories;

use App\Domain\Contents\Models\Vote;
use App\Http\ApiV1\OpenApiGenerated\Enums\VotesVoteEnum;
use Illuminate\Database\Eloquent\Factories\Factory;

class VoteFactory extends Factory
{
    protected $model = Vote::class;

    public function definition()
    {
        return [
            'post_id' => $this->faker->numberBetween(1),
            'user_id' => $this->faker->numberBetween(1),
            'vote' => $this->faker->randomElement(VotesVoteEnum::cases())->value,
        ];
    }
}
